import "./style.css"

document.getElementById("app")!.innerHTML=`
    <ul id="taskbar">
        <li>
            
        </li>
    </ul>
    <div id="terminals">
        <div class="terminal">
            <div class="titlebar">
                <span class="title">bash</span>
            </div>
            <div class="term"></div>
        </div>
        <div class="terminal">
            <div class="titlebar">
                <span class="title">bash</span>
            </div>
            <div class="term"></div>
        </div>
        <div class="terminal">
            <div class="titlebar">
                <span class="title">bash</span>
            </div>
            <div class="term" id="profile-term"></div>
        </div>
    </div>
`
let profile = document.getElementById("profile-term")
profile!.innerHTML=`<span style="color: lightgreen">~</span> $ `
for(let i = 0; i < "profile".length; i++) {
    setTimeout(()=>{profile!.innerHTML+="profile"[i]},i*100+1000)
}
setTimeout(()=>{
    profile!.innerHTML+=`
<div class="asciiart">                                               .........
                                              .~~~~.....
                                             .~~~~~.....
                                             ~~~~~~~~...
                                             :~~~~~~~~..
                                            .+::::~~~~~~
                 .                          .~~~......~~
                                                  ..~~~~
          ........                           .~~::::~~~~
      .......~~~:~                          .=++:::~~~~~
   .......~~~::++.                          ++++:::~~~~~
.......~~~::++++.                          .=+++:::~~~~~
....~~~::+++===+                          .==+++:::~~~~~
.~~:::+++=====oo.                        .==+++:::~~~~~~
~:++++====ooooo=.                        +=+++::::~~~~~~
:++===ooooooo~.                         ~=+++::::~~~~~..
+==ooooooooo.                          .=+++::::~~~~~~~.
+ooooooooo=.                          .=++++:::~~~.....
=oooooooo+                            .~::~~....        
=ooooo+..                                               
=oo+~                                                   
~.                                                       
</div>
<div class="info">
    <h3>
</div>`
},("profile".length+1)*100+1000)